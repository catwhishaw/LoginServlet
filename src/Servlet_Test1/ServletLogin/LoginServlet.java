package Servlet_Test1.ServletLogin;

import Servlet_Test1.condatabase.ConDB;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet(name = "LoginServlet")
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 设置解码方式
        String account = request.getParameter("account");
        String password = request.getParameter("password");

        // 数据库验证用户名、密码是否正确
        String msg;
        ConDB operation = new ConDB();
        if (operation.login(account, password)) {
            msg = "true";
            setCookie(response, account, password);
            setSession(request);
        } else {
            msg = "false";
        }

        operation.closeConnection();
        PrintWriter writer = response.getWriter();
        writer.print(msg);

        // 关闭流
        writer.close();
    }

    /***
     * 将成功登录的用户名密码存储到浏览器Cookie中
     * @param response 服务器响应对象
     * @param account 用户名
     * @param password 密码
     */
    private void setCookie(HttpServletResponse response, String account, String password) {
        // 若用户名密码正确，则将信息存储到cookie中
        Cookie cooUserName = new Cookie("account", account);
        // 将用户名密码存储30天
        cooUserName.setMaxAge(60 * 60 * 24 * 30);
        response.addCookie(cooUserName);
        Cookie cooPassword = new Cookie("password", password);
        cooPassword.setMaxAge(60 * 60 * 24 * 30);
        response.addCookie(cooPassword);
    }

    /***
     * 将成功登录的用户人数存储到Session中
     * @param request 客户端请求对象
     */
    private void setSession(HttpServletRequest request) {
        String visitCountKey = new String("visitCount");
        Integer visitCount = new Integer(0);
        // 如果不存在 session 会话，则创建一个 session 对象
        HttpSession session = request.getSession(true);

        // 如果第一次访问，则创建新的会话
        if (session.getAttribute(visitCountKey) == null) {
            System.out.println("user: " + session.getId() + " access server at " + ConDB.getCurrentTime());
            session.setAttribute(visitCountKey, new Integer(1));
            visitCount++;
        } else {
            visitCount = (Integer) session.getAttribute(visitCountKey);
        }

        // 检查网页上是否有新的访问者
        if (session.isNew()) {
            System.out.println("user: " + session.getId() + " access server at " + ConDB.getCurrentTime());
            visitCount = (Integer) session.getAttribute(visitCountKey);
            visitCount++;
            session.setAttribute(visitCountKey, visitCount);
        }

        System.out.println("在线人数: " + visitCount + " at " + ConDB.getCurrentTime());
    }
}
