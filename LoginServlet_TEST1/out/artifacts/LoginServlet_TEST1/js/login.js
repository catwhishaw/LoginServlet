
$(document).ready(function() {

    // 为登录按钮添加点击事件
    $("#button_log").click(function(e) {
        // 状态栏对象
        var obj_title = $(".login_title").eq(0);
        // 账号密码输入框对象
        var obj_account = $("#account");
        var obj_password = $("#password");

        // 判断账号密码是否完整
        if (obj_account.val() == "" || obj_password.val() == "") {
            obj_title.html("用户名或密码不能为空");
            // 设置新的样式
            obj_title.css("color", "#fff");
            obj_title.css("background-color", "#CC3300");
            obj_title.css("text-align", "center");
            obj_title.css("font-weight", "400");

            return;
        }

        // 传输的登录信息对象
        var login = {
            account: obj_account.val(),
            password: obj_password.val()
        }

        // 通过ajax的方式请求服务器
        $.ajax({
            type: "POST",
            url: "http://localhost:8080/LoginServlet_TEST1/login",
            data: { "account": obj_account.val(), "password": obj_password.val() },
            dataType: "text",
            success: function(response) {
                if (response == "true") {
                    obj_title.css("color", "#000000");
                    obj_title.css("background-color", "#ffffff");
                    obj_title.css("text-align", "center");
                    obj_title.css("font-weight", "400");
                    $(location).attr("href", "Loginsuccess/index.html");
                } else {
                    obj_title.html("您填写的账号名或密码错误");
                    obj_title.css("color", "#000000");
                    obj_title.css("background-color", "#ffffff");
                    obj_title.css("text-align", "center");
                    obj_title.css("font-weight", "400");
                    // 重置密码
                    obj_password.val("");
                }
            }
        });
    });

    checkCookie();
});

function checkCookie() {
    var account = getCookie("account");
    var password = getCookie("password");

    var obj_account = $("#account");
    var obj_password = $("#password");

    if (account != "") {
        obj_account.val(account);
        obj_password.val(password);
    }
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i].trim();
        if (c.indexOf(name) == 0) { return c.substring(name.length, c.length); }
    }
}